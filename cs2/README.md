# Counter-Strike 2 config

<br>
<div align="center">
  <img src="https://liquipedia.net/commons/images/thumb/e/ef/Counter-Strike_2_gamelogo_darkmode.png/800px-Counter-Strike_2_gamelogo_darkmode.png" width="500px" alt="CS2 Banner"/>

<p align="center" style="font-size: 56px; font-weight: bold; font-style: italic;">
  C O N F I G
</p>
</div>

---

## My hardware
- CPU: AMD Ryzen 7 7800X3D
- Motherboard: MSI MAG B650 Tomahawk
- Memory: Corsair Vengeance 6000MHz CL30 DDR5 2x16GB
- GPU: AMD Radeon RX 7800 XT
- OS: [NixOS Linux](https://gitlab.com/simonoscr/nixfiles)

## Launch options

### Wayland w/o gamescope

```shell
gamemoderun %command% -vulkan
```

### Wayland or X11 w gamescope

```shell
gamescope -w 1920 -h 1080 -W 3440 -H 1440 -r 165 -f -e --immediate-flips -F fsr -- gamemoderun %command% -vulkan
```

### X11 w/o gamescope

```shell
xrandr --output DP-1 --primary --mode 2560x1440 --scale-from 1440x1080 --rate 165; %command% -vulkan; xrandr --output DP-2 --primary --mode 1920x1080 --transform none --rate 144
```

## In-game video settings
Resolution: `1920x1080`

| Setting                          | Value                      	|
|----------------------------------|------------------------------|
| Boost Player Contrast            | `Enabled`                    |
| Wait for Vertical Sync           | `Disabled`                   |
| Multisampling Anti-Aliasing Mode | `2xMSAA`                     |
| Global Shadow Quality            | `Low`                        |
| Model / Texture Detail           | `LOW`   		                  |
| Texture Filtering Mode           | `Bilinear`                   |
| Shader Detail                    | `Low`                        |
| Particle Detail                  | `Low`                        |
| Ambient Occlusion                | `Disabled`                   |
| High Dynamic Range               | `Performance`                |
| FidelityFX Super Resolution      | `Disabled (Highest Quality)` |

## Shadow distance
The best thing to do is have shadows on low for performance, but override the limited draw distance in config file. I really don't like how a lot of unnecessary shadows from various stuff on the map gets drawn with high settings, I just want player shadows.

```
	"setting.csm_viewmodel_shadows"				"1"
	"setting.csm_max_shadow_dist_override"				"720"
	"setting.lb_barnlight_shadowmap_scale"				"0.400000"
	"setting.lb_enable_shadow_casting"				"1"
```

Also to decouple model texture and general texture setting to have Textures on low but things like Tracers and Helmet inpackt not as pixxelrated on texture low, set those:

```
	"setting.r_character_decal_resolution"		"512"
	"setting.r_texture_stream_max_resolution"		"1048"
	"setting.r_csgo_mboit_force_mixed_resolution"		"1"
```

> [!WARNING]
> Changing any video settings in game will overwrite these parameters.
> You can change files properties and make it read-only. You can still change settings in-game, but they dont get kept and resetting on restart if the cs_video.txt is set to rad-only.

## Symmetric Ping/Latency increase

Latency increases is just like a defacto longer internet wire (speed of wire), to try to equalize yourself into the playing field and bypass low-latency handicap algorithms in competitive games. You're downgrading your internet connection to match the other peoples' connections on the game server. And you've got the extra opportunity to also remove some ping jitter, by absorbing some of the jitter in your intentionally-latency tapedelay.

> [!IMPORTANT]
Only **symmetric latency increases are scientifically kosher** (e.g. not a cheat).
**Asymmetric latency increases is cheating**. This does mean, that any latency increases you add should be equal in the both incoming/outgoing directions.

Generally:

- A good gaming VPN (and fast VPN protocol like wireguard) can add intentional latency while bypassing bufferbloat on other backbones

- Or a Linux router is customizable to add intentional latency (packetdelay everything). Get familiar with those shell commands to add a tape delay for your packets!

So we have luck. I am on Linux so we can utilize the linux solution with the follwing commands:

### ADD delay

```shell
tc qdisc add dev [networkdevice] root netem delay 20ms
```

### CHANGE delay

```shell
tc qdisc change dev [networkdevice] root netem delay 20ms
```

### REMOVE delay

```shell
tc qdisc del dev [networkdevice] root netem
```
